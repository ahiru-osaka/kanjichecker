﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace WindowsFormsApplication3
{
    public partial class FileSelectWindow : Form
    {
        private string file_path;      //読み込んだファイルのパスを保存する
        private string file_name;      //読み込んだファイルのファイル名を保存する

        //ファイルパスからファイル名を取得する.

        private string file_path_to_file_name(string _file_path)
        {
            return Path.GetFileName(_file_path);
        }

        public FileSelectWindow()
        {
            file_path = "";
            file_name = "";
            InitializeComponent();
            string[] files = System.Environment.GetCommandLineArgs();

            if(files.Length > 1)
            {
                file_path = files[1];
                file_name = file_path_to_file_name(file_path);
                textBox1.Text = file_name;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.FileName = "default";
            dialog.Filter = "csv File(.csv) | *.csv|Json File(.json) | *.json|すべてのファイル(*.*)|*.*";
            dialog.Title = "ファイルを選択してください";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                file_path = dialog.FileName;
                file_name = file_path_to_file_name(file_path);
                textBox1.Text = file_name;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (file_path != "" && comboBox1.SelectedItem != null)
            {
                KanjiCheckWindow window = new KanjiCheckWindow(file_path, comboBox1.SelectedItem.ToString());
                window.ShowDialog();

            }
            else 
            {
                if (comboBox1.SelectedItem != null) MessageBox.Show("ファイルを選択してください", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (file_path != "") MessageBox.Show("学年を選択してください", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else MessageBox.Show("ファイルと学年を選択してください", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /*D&Dされた時のイベントハンドラ*/
        private void textBox1_DragDrop(object sender, DragEventArgs e)
        {
            //D&Dされたファイルのpathを保存する
            string[] dialog_file_path_array = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            file_path = dialog_file_path_array[0];
            file_name = file_path_to_file_name(file_path);
            textBox1.Text = file_name;
        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }
    }
}
